﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCode_To_SVG
{
    class Point
    {
        private int x;
        private int y;
        public int X
        {
            get { return x; }
        }
        public int Y
        {
            get { return y; }
        }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void MoveLeft()
        {
            x--;
        }

        public void MoveRigth()
        {
            x++;
        }

        public void MoveUp()
        {
            y--;
        }

        public void MoveDown()
        {
            y++;
        }

        public override string ToString()
        {
            return  x + "," + y;
        }

        /*public static Point operator *(Point p, double scalar)
        {
            return new Point(p.x * scalar, p.y*scalar);
        }*/
    }
}
