﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCode_To_SVG
{
    class Color
    {
        private byte[] rgb;
        public byte R
        {
            get { return rgb[0]; }
        }
        public byte G
        {
            get { return rgb[1]; }
        }
        public byte B
        {
            get { return rgb[2]; }
        }

        public Color(byte r, byte g, byte b)
        {
            rgb = new byte[3];
            rgb[0] = r;
            rgb[1] = g;
            rgb[2] = b;
        }

        public Color(string hex)
        {
            if (hex.Length > 6)
                //hex = hex.Substring(hex.Length - 6, 6);
                hex = "000000";

            rgb = new byte[3];
            //Debug.WriteLine(hex);
            for (int i = 0; i < 3; i++)
            {
                string hexString = hex.Substring(i * 2, 2);
                rgb[i] = (byte) Int32.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
            }
        }

        public override string ToString()
        {
            string hex = BitConverter.ToString(rgb);
            return "#"+hex.Replace("-", "");
        }
    }
}
