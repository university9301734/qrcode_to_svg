﻿using System;
using System.Diagnostics;
using ZXing.QrCode.Internal;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCode_To_SVG
{
    class Options
    {
        //general options
        private string inputPath;
        private string outputPath;

        public enum UnitMeasure {MM=0, CM=1, PX=2, NONE=3}
        private UnitMeasure measure;
        private Size totalSize;
        private Size qrdisposition;
        private Size qrseparation;

        private Color[] strokes;
        private bool[] fill;

        //qr options
        private Size qrsize;
        private ErrorCorrectionLevel err;

        //necklace options
        private Size necklaceSize;

        //-------------------------------------------------------------properties-------------------------------------------------------------------------
        public UnitMeasure Measure
        {
            get { return measure; }
            set { measure = value; }
        }
        public string InputPath
        {
            get { return inputPath; }
            set { inputPath = value; }
        }
        public string OutputPath
        {
            get { return outputPath; }
            set {outputPath = value; }
        }
        public Size SheetSize
        {
            get { return totalSize; }
            set { totalSize = value; }
        }
        public Color[] Strokes
        {
            get { return strokes; }
            set { strokes = value; }
        }
        public bool[] Fill
        {
            get { return fill; }
            set { fill = value; }
        }
        public Size QRSize
        {
            get { return qrsize; }
            set { qrsize = value; }
        }
        public ErrorCorrectionLevel QRError
        {
            get { return err; }
            set { err = value; }
        }
        public Size NecklaceSize
        {
            get { return necklaceSize; }
            set { necklaceSize = value; }
        }
        public Size QRDispoition
        {
            get { return qrdisposition; }
            set { qrdisposition = value; }
        }
        public Size QRSeparation
        {
            get { return qrseparation; }
            set { qrseparation = value; }
        }


        //-------------------------------------------------------------methods------------------------------------------------------------------------------
        public Options(string configFilePath, string inputPath, string outputPath)
        {
            this.inputPath = inputPath;
            this.outputPath = outputPath;

            //defualt options in case anything goes wrong with files
            SheetSize = new Size(1000, 700, Size.UnitOfMeasure.MM);
            Measure = UnitMeasure.MM;
            qrdisposition = new Size(4, 1);
            qrseparation = new Size(1,1);
            QRSize = new Size(8, 8);
            NecklaceSize = new Size(12, 9);
            QRError = ErrorCorrectionLevel.L;
            Fill = new bool[4];
            strokes = new Color[4];
            strokes[0] = new Color("000000");
            strokes[1] = new Color("0000FF");
            strokes[2] = new Color("FF0000");
            strokes[3] = new Color("FFFFFF");

            ParseConfigFile(configFilePath);
        }

        private void ParseConfigFile(string path)
        {
            if (!File.Exists(path))
                return;

            StreamReader f = new StreamReader(path);
            using (f)
            {
                while (!f.EndOfStream)
                {
                    string line = f.ReadLine();
                    int i = line.IndexOf(":");
                    string c = line.Substring(0, i);
                    NextToken(line, out line); //consume the identifier c
                    switch (c)
                    {
                        case "UNIT_MEASURE":
                            string k = NextToken(line, out line);
                            switch (k)
                            {
                                case "MM":
                                    measure = UnitMeasure.MM;
                                    break;
                                case "CM":
                                    measure = UnitMeasure.CM;
                                    break;
                                case "PX":
                                    measure = UnitMeasure.PX;
                                    break;
                                case "NONE":
                                    measure = UnitMeasure.NONE;
                                    break;
                            }
                            break;
                        case "SHEET_SIZE":
                            totalSize = new Size(Double.Parse(NextToken(line, out line)), Double.Parse(NextToken(line, out line)), NextToken(line, out line));
                            break;
                        case "STROKES":
                            strokes = new Color[4];
                            strokes[0] = new Color(NextToken(line, out line).Substring(1, 6));
                            strokes[1] = new Color(NextToken(line, out line).Substring(1, 6));
                            strokes[2] = new Color(NextToken(line, out line).Substring(1, 6));
                            strokes[3] = new Color(NextToken(line, out line).Substring(1,6));
                            break;
                        case "FILL":
                            fill[0] = Boolean.Parse(NextToken(line, out line));
                            fill[1] = Boolean.Parse(NextToken(line, out line));
                            fill[2] = Boolean.Parse(NextToken(line, out line));
                            fill[3] = Boolean.Parse(NextToken(line, out line));
                            break;
                        case "QR_SIZE":
                            qrsize = new Size(Double.Parse(NextToken(line, out line)), Double.Parse(NextToken(line, out line)));
                            break;
                        case "QR_ERROR":
                            string t = NextToken(line, out line);
                            switch (t)
                            {
                                case "L":
                                    err = ErrorCorrectionLevel.L;
                                    break;
                                case "M":
                                    err = ErrorCorrectionLevel.M;
                                    break;
                                case "Q":
                                    err = ErrorCorrectionLevel.Q;
                                    break;
                                case "H":
                                    err = ErrorCorrectionLevel.H;
                                    break;
                            }
                            break;
                        case "NECKLACE_SIZE":
                            necklaceSize = new Size(Double.Parse(NextToken(line, out line)), Double.Parse(NextToken(line, out line)));
                            break;
                        case "QR_DISPOSITION":
                            qrdisposition = new Size(Double.Parse(NextToken(line, out line)), Double.Parse(NextToken(line, out line)));
                            break;
                        case "QR_SEPARATION":
                            qrseparation = new Size(Double.Parse(NextToken(line, out line)), Double.Parse(NextToken(line, out line)));
                            break;
                    }
                }
            }
        }

        public bool SaveOptions(string path)
        {
            StreamWriter w = null;
            try
            {
                if (!File.Exists(path))
                    w = File.CreateText(path);
                else
                    w = new StreamWriter(path);
            }
            catch (IOException e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }

            using (w)
            {
                string tmp = "MM";
                if (measure == UnitMeasure.MM)
                    tmp = "MM";
                else if (measure == UnitMeasure.CM)
                    tmp = "CM";
                else if (measure == UnitMeasure.PX)
                    tmp = "PX";
                else if (measure == UnitMeasure.NONE)
                    tmp = "NONE";
                w.WriteLine("UNIT_MEASURE: " + tmp);
                tmp = totalSize.ToString();
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace(",", " ");
                w.WriteLine("SHEET_SIZE: " + tmp);
                w.WriteLine("STROKES: " + strokes[0].ToString() + " " + strokes[1].ToString() + " " + strokes[2].ToString() + " " + strokes[3].ToString());
                w.WriteLine("FILL: " + fill[0] + " " + fill[1] + " " + fill[2] + " " + fill[3]);
                tmp = qrsize.ToString();
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace(",", " ");
                w.WriteLine("QR_SIZE: " + tmp);
                if (err == ErrorCorrectionLevel.L)
                    tmp = "L";
                else if (err == ErrorCorrectionLevel.M)
                    tmp = "M";
                else if (err == ErrorCorrectionLevel.H)
                    tmp = "H";
                else if (err == ErrorCorrectionLevel.Q)
                    tmp = "Q";
                w.WriteLine("QR_ERROR: " + tmp);
                tmp = necklaceSize.ToString();
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace(",", " ");
                w.WriteLine("NECKLACE_SIZE: " + tmp);
                tmp = qrdisposition.ToString();
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace(",", " ");
                w.WriteLine("QR_DISPOSITION: " + tmp);
                tmp = qrseparation.ToString();
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace(",", " ");
                w.WriteLine("QR_SEPARATION: " + tmp);
            }

            return true;
        }

        private string NextToken(string s, out string consumed)
        {
            s = s.Trim();
            int i = s.IndexOf(" ");
            if (i > 0)
            {
                consumed = s.Substring(i, s.Length - i);
                return s.Substring(0, i);
            }
            else
            { 
                consumed = "";
                return s;
            }
        }

    }
}
