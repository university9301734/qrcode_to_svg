﻿using System;
using System.IO;
using ZXing.QrCode.Internal;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace QRCode_To_SVG
{
    public partial class Form1 : Form
    {
        private string defaultInputPath;
        private string defaultOutputPath;
        private string configPath;
        private Size sheet;
        private Size RowCols;
        private Size separation;
        private Size neck;
        private Size qr;
        private Options op;

        public Form1()
        {
            configPath = Path.Combine(Environment.CurrentDirectory, "config.txt");
            defaultInputPath = Path.Combine(Environment.CurrentDirectory, "InputFileExample.txt");
            string date = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
            defaultOutputPath = Path.Combine(Environment.CurrentDirectory, "OUTPUT", date);
            op = new Options(configPath, defaultInputPath, defaultOutputPath);

            InitializeComponent();
            this.Text = "QR to SVG"; //change the title to the form
            Error_list.DropDownStyle = ComboBoxStyle.DropDownList;
            Unit_list.DropDownStyle = ComboBoxStyle.DropDownList;
            sheet = op.SheetSize;
            RowCols = op.QRDispoition;
            separation = op.QRSeparation;
            neck = op.NecklaceSize;
            qr = op.QRSize;
            Unit_list.SelectedIndex = (int)op.Measure;
            Error_list.SelectedIndex = op.QRError.ordinal(); //???
            InputFile_label.Text = defaultInputPath;
            InputLabel_tooltip.SetToolTip(InputFile_label, InputFile_label.Text);
            outputDir_label.Text = defaultOutputPath;
            OutputDir_tooltip.SetToolTip(outputDir_label, outputDir_label.Text);
            SheetDimensionX.Value = (decimal)sheet.X;
            SheetDimensionY.Value = (decimal)sheet.Y;
            RowsDisposition.Value = (decimal)(RowCols.X > 0 ? RowCols.X : 1);
            ColsDisposition.Value = (decimal)(RowCols.Y > 0 ? RowCols.Y : 1);
            SeparationX.Value = (decimal)separation.X;
            SeparationY.Value = (decimal)separation.Y;
            NecklaceX.Value = (decimal)neck.X;
            NecklaceY.Value = (decimal)neck.Y;
            QrY.Value = (decimal)qr.Y;
            Stroke0_box.Text = op.Strokes[0].ToString().Substring(1, 6);
            Stroke1_box.Text = op.Strokes[1].ToString().Substring(1, 6);
            Stroke2_box.Text = op.Strokes[2].ToString().Substring(1, 6);
            Stroke3_box.Text = op.Strokes[3].ToString().Substring(1, 6);
            checkBox0.Checked = op.Fill[0];
            checkBox1.Checked = op.Fill[1];
            checkBox2.Checked = op.Fill[2];
            checkBox3.Checked = op.Fill[3];
            ValidateChildren(); //to correctly display hex values on stroke boxes
        }


        private void Start_button_Click(object sender, EventArgs e)
        {
            UpdateOptions();
            SVGManager man = new SVGManager(op);
            man.WriteSVG();
        }

        private void DefaultInput_button_Click(object sender, EventArgs e)
        {
            defaultInputPath = Path.Combine(Environment.CurrentDirectory, "InputFileExample.txt");
            InputFile_label.Text = defaultInputPath;
            InputLabel_tooltip.SetToolTip(InputFile_label, InputFile_label.Text);
        }

        private void BrowseInput_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog()
            {
                InitialDirectory = Environment.CurrentDirectory,
                Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            if (f.ShowDialog() == DialogResult.OK)
            {
                op.InputPath = f.FileName;
                InputFile_label.Text = op.InputPath;
                InputLabel_tooltip.SetToolTip(InputFile_label, InputFile_label.Text);
            }
        }

        private void DefaultOutput_button_Click(object sender, EventArgs e)
        {
            string date = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
            defaultOutputPath = Path.Combine(Environment.CurrentDirectory, "OUTPUT", date);
            outputDir_label.Text = defaultOutputPath;
            OutputDir_tooltip.SetToolTip(outputDir_label, outputDir_label.Text);
        }

        private void BrowseOutput_button_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                op.OutputPath = f.SelectedPath;
                outputDir_label.Text = op.OutputPath;
                OutputDir_tooltip.SetToolTip(outputDir_label, outputDir_label.Text);
            }
        }

        private void Unit_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            op.Measure = (Options.UnitMeasure)Unit_list.SelectedIndex;
            //Debug.WriteLine("selected "+Unit_list.SelectedIndex);
        }

        private void SheetDimensionX_ValueChanged(object sender, EventArgs e)
        {
            sheet.X = (double)SheetDimensionX.Value;
            //Debug.WriteLine(sheet.X);
        }

        private void SheetDimensionY_ValueChanged(object sender, EventArgs e)
        {
            sheet.Y = (double)SheetDimensionY.Value;
        }

        private void RowsDisposition_ValueChanged(object sender, EventArgs e)
        {
            RowCols.X = (int)RowsDisposition.Value;
        }

        private void ColsDisposition_ValueChanged(object sender, EventArgs e)
        {
            RowCols.Y = (int)ColsDisposition.Value;
        }

        private void SeparationX_ValueChanged(object sender, EventArgs e)
        {
            separation.X = (double)SeparationX.Value;
        }

        private void SeparationY_ValueChanged(object sender, EventArgs e)
        {
            separation.Y = (double)SeparationY.Value;
        }

        private void NecklaceX_ValueChanged(object sender, EventArgs e)
        {
            neck.X = (double)NecklaceX.Value;
        }

        private void NecklaceY_ValueChanged(object sender, EventArgs e)
        {
            neck.Y = (double)NecklaceY.Value;
        }

        private void QrX_ValueChanged(object sender, EventArgs e)
        {
            qr.X = (double)QrY.Value;
        }

        private void QrY_ValueChanged(object sender, EventArgs e)
        {
            qr.Y = (double)QrY.Value;
            qr.X = (double)QrY.Value;
        }

        private void Error_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (Error_list.SelectedIndex)
            {
                case 0:
                    op.QRError = ErrorCorrectionLevel.L;
                    break;
                case 1:
                    op.QRError = ErrorCorrectionLevel.M;
                    break;
                case 2:
                    op.QRError = ErrorCorrectionLevel.Q;
                    break;
                case 3:
                    op.QRError = ErrorCorrectionLevel.H;
                    break;
            }
            //Debug.WriteLine(op.QRError);
        }

        private void Stroke0_box_Validated(object sender, EventArgs e)
        {
           

            string text = Stroke0_box.Text.Replace(":", "").ToUpper();
            if (text.Length != 6 || FindIncorrectHexValue(text))
            {
                text = op.Strokes[0].ToString().Substring(1, 6);
            }
            else
                op.Strokes[0] = new Color(text);

            Stroke0_box.Text = text.Substring(0, 2)+":";
            Stroke0_box.Text += text.Substring(2, 2) + ":";
            Stroke0_box.Text += text.Substring(4, 2);
        }

        private void Stroke1_box_Validated(object sender, EventArgs e)
        {
            string text = Stroke1_box.Text.Replace(":", "").ToUpper();
            if (text.Length != 6 || FindIncorrectHexValue(text))
            {
                text = op.Strokes[1].ToString().Substring(1, 6);
            }
            else
                op.Strokes[1] = new Color(text);

            Stroke1_box.Text = text.Substring(0, 2) + ":";
            Stroke1_box.Text += text.Substring(2, 2) + ":";
            Stroke1_box.Text += text.Substring(4, 2);
        }

        private void Stroke2_box_Validated(object sender, EventArgs e)
        {
            string text = Stroke2_box.Text.Replace(":", "").ToUpper();
            if (text.Length != 6 || FindIncorrectHexValue(text))
            {
                text = op.Strokes[2].ToString().Substring(1, 6);
            }
            else
                op.Strokes[2] = new Color(text);

            Stroke2_box.Text = text.Substring(0, 2) + ":";
            Stroke2_box.Text += text.Substring(2, 2) + ":";
            Stroke2_box.Text += text.Substring(4, 2);
        }

        private void Stroke3_box_Validated(object sender, EventArgs e)
        {
            string text = Stroke3_box.Text.Replace(":", "").ToUpper();
            if (text.Length != 6 || FindIncorrectHexValue(text))
            {
                text = op.Strokes[3].ToString().Substring(1, 6);
            }
            else
                op.Strokes[3] = new Color(text);

            Stroke3_box.Text = text.Substring(0, 2) + ":";
            Stroke3_box.Text += text.Substring(2, 2) + ":";
            Stroke3_box.Text += text.Substring(4, 2);
        }

        private void checkBox0_CheckedChanged(object sender, EventArgs e)
        {
            op.Fill[0] = checkBox0.Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            op.Fill[1] = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            op.Fill[2] = checkBox2.Checked;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            op.Fill[3] = checkBox3.Checked;
        }

        private bool FindIncorrectHexValue(string s)
        {
            string correctCases = "1234567890ABCDEF";
            for (int i = 0; i < s.Length; i++)
            {
                if (correctCases.IndexOf(s.ElementAt(i)) < 0)
                    return true;
            }
            return false;
        }

        private void SaveOptions_button_Click(object sender, EventArgs e)
        {
            UpdateOptions();
            op.SaveOptions(configPath);        
        }

        private void UpdateOptions()
        {
            op.SheetSize = sheet;
            op.QRDispoition = RowCols;
            op.QRSeparation = separation;
            op.QRSize = qr;
            op.NecklaceSize = neck;
        }
    }
}
