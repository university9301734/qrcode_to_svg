﻿namespace QRCode_To_SVG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BackgroudLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.QrY = new System.Windows.Forms.NumericUpDown();
            this.NecklaceY = new System.Windows.Forms.NumericUpDown();
            this.NecklaceX = new System.Windows.Forms.NumericUpDown();
            this.SeparationY = new System.Windows.Forms.NumericUpDown();
            this.SeparationX = new System.Windows.Forms.NumericUpDown();
            this.ColsDisposition = new System.Windows.Forms.NumericUpDown();
            this.RowsDisposition = new System.Windows.Forms.NumericUpDown();
            this.SheetDimensionY = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.Stroke0_box = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.Stroke1_box = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.Stroke2_box = new System.Windows.Forms.TextBox();
            this.DefaultOutput_button = new System.Windows.Forms.Button();
            this.outputDir_label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InputFile_label = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DefaultInput_button = new System.Windows.Forms.Button();
            this.BrowseInput_button = new System.Windows.Forms.Button();
            this.BrowseOutput_button = new System.Windows.Forms.Button();
            this.Start_button = new System.Windows.Forms.Button();
            this.SaveOptions_button = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.Stroke3_box = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkBox0 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.SheetDimensionX = new System.Windows.Forms.NumericUpDown();
            this.Unit_list = new System.Windows.Forms.ComboBox();
            this.Error_list = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.InputLabel_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.OutputDir_tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.BackgroudLayout.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QrY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NecklaceY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NecklaceX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeparationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeparationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColsDisposition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RowsDisposition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SheetDimensionY)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SheetDimensionX)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackgroudLayout
            // 
            this.BackgroudLayout.ColumnCount = 2;
            this.BackgroudLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 600F));
            this.BackgroudLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.BackgroudLayout.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.BackgroudLayout.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.BackgroudLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackgroudLayout.Location = new System.Drawing.Point(0, 0);
            this.BackgroudLayout.Name = "BackgroudLayout";
            this.BackgroudLayout.RowCount = 2;
            this.BackgroudLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 570F));
            this.BackgroudLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.BackgroudLayout.Size = new System.Drawing.Size(1324, 587);
            this.BackgroudLayout.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.80952F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.04762F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.04762F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.04762F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.04762F));
            this.tableLayoutPanel1.Controls.Add(this.QrY, 5, 11);
            this.tableLayoutPanel1.Controls.Add(this.NecklaceY, 5, 10);
            this.tableLayoutPanel1.Controls.Add(this.NecklaceX, 4, 10);
            this.tableLayoutPanel1.Controls.Add(this.SeparationY, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.SeparationX, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.ColsDisposition, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.RowsDisposition, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.SheetDimensionY, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 4, 13);
            this.tableLayoutPanel1.Controls.Add(this.DefaultOutput_button, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.outputDir_label, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.InputFile_label, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label9, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.DefaultInput_button, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.BrowseInput_button, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.BrowseOutput_button, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.Start_button, 5, 16);
            this.tableLayoutPanel1.Controls.Add(this.SaveOptions_button, 4, 16);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.label11, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 5, 13);
            this.tableLayoutPanel1.Controls.Add(this.label18, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.checkBox0, 2, 14);
            this.tableLayoutPanel1.Controls.Add(this.checkBox1, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.checkBox2, 4, 14);
            this.tableLayoutPanel1.Controls.Add(this.checkBox3, 5, 14);
            this.tableLayoutPanel1.Controls.Add(this.SheetDimensionX, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.Unit_list, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.Error_list, 5, 12);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 17;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.24997F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.249972F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.247471F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.252931F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(594, 564);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // QrY
            // 
            this.QrY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.QrY.Location = new System.Drawing.Point(485, 366);
            this.QrY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.QrY.Name = "QrY";
            this.QrY.Size = new System.Drawing.Size(106, 20);
            this.QrY.TabIndex = 41;
            this.QrY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.QrY.ThousandsSeparator = true;
            this.QrY.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.QrY.ValueChanged += new System.EventHandler(this.QrY_ValueChanged);
            // 
            // NecklaceY
            // 
            this.NecklaceY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NecklaceY.Location = new System.Drawing.Point(485, 333);
            this.NecklaceY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NecklaceY.Name = "NecklaceY";
            this.NecklaceY.Size = new System.Drawing.Size(106, 20);
            this.NecklaceY.TabIndex = 39;
            this.NecklaceY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NecklaceY.ThousandsSeparator = true;
            this.NecklaceY.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.NecklaceY.ValueChanged += new System.EventHandler(this.NecklaceY_ValueChanged);
            // 
            // NecklaceX
            // 
            this.NecklaceX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NecklaceX.Location = new System.Drawing.Point(374, 333);
            this.NecklaceX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.NecklaceX.Name = "NecklaceX";
            this.NecklaceX.Size = new System.Drawing.Size(105, 20);
            this.NecklaceX.TabIndex = 38;
            this.NecklaceX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NecklaceX.ThousandsSeparator = true;
            this.NecklaceX.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.NecklaceX.ValueChanged += new System.EventHandler(this.NecklaceX_ValueChanged);
            // 
            // SeparationY
            // 
            this.SeparationY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SeparationY.Location = new System.Drawing.Point(485, 300);
            this.SeparationY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.SeparationY.Name = "SeparationY";
            this.SeparationY.Size = new System.Drawing.Size(106, 20);
            this.SeparationY.TabIndex = 37;
            this.SeparationY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SeparationY.ThousandsSeparator = true;
            this.SeparationY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SeparationY.ValueChanged += new System.EventHandler(this.SeparationY_ValueChanged);
            // 
            // SeparationX
            // 
            this.SeparationX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SeparationX.Location = new System.Drawing.Point(374, 300);
            this.SeparationX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.SeparationX.Name = "SeparationX";
            this.SeparationX.Size = new System.Drawing.Size(105, 20);
            this.SeparationX.TabIndex = 36;
            this.SeparationX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SeparationX.ThousandsSeparator = true;
            this.SeparationX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SeparationX.ValueChanged += new System.EventHandler(this.SeparationX_ValueChanged);
            // 
            // ColsDisposition
            // 
            this.ColsDisposition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ColsDisposition.Location = new System.Drawing.Point(485, 267);
            this.ColsDisposition.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.ColsDisposition.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ColsDisposition.Name = "ColsDisposition";
            this.ColsDisposition.Size = new System.Drawing.Size(106, 20);
            this.ColsDisposition.TabIndex = 35;
            this.ColsDisposition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ColsDisposition.ThousandsSeparator = true;
            this.ColsDisposition.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ColsDisposition.ValueChanged += new System.EventHandler(this.ColsDisposition_ValueChanged);
            // 
            // RowsDisposition
            // 
            this.RowsDisposition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RowsDisposition.Location = new System.Drawing.Point(374, 267);
            this.RowsDisposition.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.RowsDisposition.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.RowsDisposition.Name = "RowsDisposition";
            this.RowsDisposition.Size = new System.Drawing.Size(105, 20);
            this.RowsDisposition.TabIndex = 34;
            this.RowsDisposition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.RowsDisposition.ThousandsSeparator = true;
            this.RowsDisposition.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.RowsDisposition.ValueChanged += new System.EventHandler(this.RowsDisposition_ValueChanged);
            // 
            // SheetDimensionY
            // 
            this.SheetDimensionY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SheetDimensionY.Location = new System.Drawing.Point(485, 234);
            this.SheetDimensionY.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.SheetDimensionY.Name = "SheetDimensionY";
            this.SheetDimensionY.Size = new System.Drawing.Size(106, 20);
            this.SheetDimensionY.TabIndex = 33;
            this.SheetDimensionY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SheetDimensionY.ThousandsSeparator = true;
            this.SheetDimensionY.Value = new decimal(new int[] {
            700,
            0,
            0,
            0});
            this.SheetDimensionY.ValueChanged += new System.EventHandler(this.SheetDimensionY_ValueChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.69811F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.30189F));
            this.tableLayoutPanel6.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.Stroke0_box, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(152, 432);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(105, 27);
            this.tableLayoutPanel6.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 27);
            this.label17.TabIndex = 0;
            this.label17.Text = "1:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stroke0_box
            // 
            this.Stroke0_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Stroke0_box.Location = new System.Drawing.Point(25, 3);
            this.Stroke0_box.Name = "Stroke0_box";
            this.Stroke0_box.Size = new System.Drawing.Size(77, 20);
            this.Stroke0_box.TabIndex = 1;
            this.Stroke0_box.Text = "00:00:00";
            this.Stroke0_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Stroke0_box.Validated += new System.EventHandler(this.Stroke0_box_Validated);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.69811F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.30189F));
            this.tableLayoutPanel5.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.Stroke1_box, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(263, 432);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(105, 27);
            this.tableLayoutPanel5.TabIndex = 25;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 27);
            this.label16.TabIndex = 0;
            this.label16.Text = "2:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stroke1_box
            // 
            this.Stroke1_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Stroke1_box.Location = new System.Drawing.Point(25, 3);
            this.Stroke1_box.Name = "Stroke1_box";
            this.Stroke1_box.Size = new System.Drawing.Size(77, 20);
            this.Stroke1_box.TabIndex = 1;
            this.Stroke1_box.Text = "00:00:FF";
            this.Stroke1_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Stroke1_box.Validated += new System.EventHandler(this.Stroke1_box_Validated);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.69811F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.30189F));
            this.tableLayoutPanel4.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Stroke2_box, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(374, 432);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(105, 27);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 27);
            this.label15.TabIndex = 0;
            this.label15.Text = "3:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stroke2_box
            // 
            this.Stroke2_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Stroke2_box.Location = new System.Drawing.Point(25, 3);
            this.Stroke2_box.Name = "Stroke2_box";
            this.Stroke2_box.Size = new System.Drawing.Size(77, 20);
            this.Stroke2_box.TabIndex = 1;
            this.Stroke2_box.Text = "FF:00:00";
            this.Stroke2_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Stroke2_box.Validated += new System.EventHandler(this.Stroke2_box_Validated);
            // 
            // DefaultOutput_button
            // 
            this.DefaultOutput_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DefaultOutput_button.Location = new System.Drawing.Point(374, 102);
            this.DefaultOutput_button.Name = "DefaultOutput_button";
            this.DefaultOutput_button.Size = new System.Drawing.Size(105, 27);
            this.DefaultOutput_button.TabIndex = 15;
            this.DefaultOutput_button.Text = "Default";
            this.DefaultOutput_button.UseVisualStyleBackColor = true;
            this.DefaultOutput_button.Click += new System.EventHandler(this.DefaultOutput_button_Click);
            // 
            // outputDir_label
            // 
            this.outputDir_label.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.outputDir_label, 3);
            this.outputDir_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputDir_label.Location = new System.Drawing.Point(13, 99);
            this.outputDir_label.Name = "outputDir_label";
            this.outputDir_label.Size = new System.Drawing.Size(355, 33);
            this.outputDir_label.TabIndex = 5;
            this.outputDir_label.Text = "output directory";
            this.outputDir_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "INPUT FILE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label2, 2);
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "OUTPUT PATH";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label3, 2);
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "OPTIONS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // InputFile_label
            // 
            this.InputFile_label.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.InputFile_label, 3);
            this.InputFile_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InputFile_label.Location = new System.Drawing.Point(13, 33);
            this.InputFile_label.Name = "InputFile_label";
            this.InputFile_label.Size = new System.Drawing.Size(355, 33);
            this.InputFile_label.TabIndex = 4;
            this.InputFile_label.Text = "input file";
            this.InputFile_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Unit of measure";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 26);
            this.label5.TabIndex = 7;
            this.label5.Text = "Total sheet dimensions\r\n(X / Y)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 26);
            this.label6.TabIndex = 8;
            this.label6.Text = "Necklaces disposition \r\n(Rows/ Cols)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 300);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 26);
            this.label7.TabIndex = 9;
            this.label7.Text = "Necklaces separation \r(top-bottom / left-right)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 333);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 26);
            this.label10.TabIndex = 12;
            this.label10.Text = "Necklace dimensions\r\n(X / Y)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 406);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "QR error correction level";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 366);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 26);
            this.label8.TabIndex = 10;
            this.label8.Text = "QR dimension\r\n(X / Y)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DefaultInput_button
            // 
            this.DefaultInput_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DefaultInput_button.Location = new System.Drawing.Point(374, 36);
            this.DefaultInput_button.Name = "DefaultInput_button";
            this.DefaultInput_button.Size = new System.Drawing.Size(105, 27);
            this.DefaultInput_button.TabIndex = 14;
            this.DefaultInput_button.Text = "Default";
            this.DefaultInput_button.UseVisualStyleBackColor = true;
            this.DefaultInput_button.Click += new System.EventHandler(this.DefaultInput_button_Click);
            // 
            // BrowseInput_button
            // 
            this.BrowseInput_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowseInput_button.Location = new System.Drawing.Point(485, 36);
            this.BrowseInput_button.Name = "BrowseInput_button";
            this.BrowseInput_button.Size = new System.Drawing.Size(106, 27);
            this.BrowseInput_button.TabIndex = 16;
            this.BrowseInput_button.Text = "Browse";
            this.BrowseInput_button.UseVisualStyleBackColor = true;
            this.BrowseInput_button.Click += new System.EventHandler(this.BrowseInput_button_Click);
            // 
            // BrowseOutput_button
            // 
            this.BrowseOutput_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowseOutput_button.Location = new System.Drawing.Point(485, 102);
            this.BrowseOutput_button.Name = "BrowseOutput_button";
            this.BrowseOutput_button.Size = new System.Drawing.Size(106, 27);
            this.BrowseOutput_button.TabIndex = 17;
            this.BrowseOutput_button.Text = "Browse";
            this.BrowseOutput_button.UseVisualStyleBackColor = true;
            this.BrowseOutput_button.Click += new System.EventHandler(this.BrowseOutput_button_Click);
            // 
            // Start_button
            // 
            this.Start_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Start_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Start_button.Location = new System.Drawing.Point(485, 531);
            this.Start_button.Name = "Start_button";
            this.Start_button.Size = new System.Drawing.Size(106, 30);
            this.Start_button.TabIndex = 18;
            this.Start_button.Text = "START";
            this.Start_button.UseVisualStyleBackColor = true;
            this.Start_button.Click += new System.EventHandler(this.Start_button_Click);
            // 
            // SaveOptions_button
            // 
            this.SaveOptions_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveOptions_button.Location = new System.Drawing.Point(374, 531);
            this.SaveOptions_button.Name = "SaveOptions_button";
            this.SaveOptions_button.Size = new System.Drawing.Size(105, 30);
            this.SaveOptions_button.TabIndex = 19;
            this.SaveOptions_button.Text = "Save Options";
            this.SaveOptions_button.UseVisualStyleBackColor = true;
            this.SaveOptions_button.Click += new System.EventHandler(this.SaveOptions_button_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.SetColumnSpan(this.progressBar1, 3);
            this.progressBar1.Location = new System.Drawing.Point(13, 532);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(355, 27);
            this.progressBar1.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 432);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 26);
            this.label11.TabIndex = 13;
            this.label11.Text = "Strokes color\r\n(RGB hexadecimal)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.69811F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.30189F));
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Stroke3_box, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(485, 432);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(106, 27);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 27);
            this.label14.TabIndex = 0;
            this.label14.Text = "4:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Stroke3_box
            // 
            this.Stroke3_box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Stroke3_box.Location = new System.Drawing.Point(25, 3);
            this.Stroke3_box.Name = "Stroke3_box";
            this.Stroke3_box.Size = new System.Drawing.Size(78, 20);
            this.Stroke3_box.TabIndex = 1;
            this.Stroke3_box.Text = "FF:FF:FF";
            this.Stroke3_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Stroke3_box.Validated += new System.EventHandler(this.Stroke3_box_Validated);
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 472);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "Fill strokes?";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBox0
            // 
            this.checkBox0.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox0.AutoSize = true;
            this.checkBox0.Location = new System.Drawing.Point(242, 471);
            this.checkBox0.Name = "checkBox0";
            this.checkBox0.Size = new System.Drawing.Size(15, 14);
            this.checkBox0.TabIndex = 28;
            this.checkBox0.UseVisualStyleBackColor = true;
            this.checkBox0.CheckedChanged += new System.EventHandler(this.checkBox0_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(353, 471);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 29;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(464, 471);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 30;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(576, 471);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 31;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // SheetDimensionX
            // 
            this.SheetDimensionX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SheetDimensionX.Location = new System.Drawing.Point(374, 234);
            this.SheetDimensionX.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.SheetDimensionX.Name = "SheetDimensionX";
            this.SheetDimensionX.Size = new System.Drawing.Size(105, 20);
            this.SheetDimensionX.TabIndex = 32;
            this.SheetDimensionX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SheetDimensionX.ThousandsSeparator = true;
            this.SheetDimensionX.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SheetDimensionX.ValueChanged += new System.EventHandler(this.SheetDimensionX_ValueChanged);
            // 
            // Unit_list
            // 
            this.Unit_list.BackColor = System.Drawing.SystemColors.Window;
            this.Unit_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Unit_list.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Unit_list.FormattingEnabled = true;
            this.Unit_list.Items.AddRange(new object[] {
            "mm",
            "cm",
            "pixel",
            "none"});
            this.Unit_list.Location = new System.Drawing.Point(485, 201);
            this.Unit_list.Name = "Unit_list";
            this.Unit_list.Size = new System.Drawing.Size(106, 21);
            this.Unit_list.TabIndex = 42;
            this.Unit_list.SelectedIndexChanged += new System.EventHandler(this.Unit_list_SelectedIndexChanged);
            // 
            // Error_list
            // 
            this.Error_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Error_list.FormattingEnabled = true;
            this.Error_list.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Error_list.Items.AddRange(new object[] {
            "L   (7%)",
            "M (15%)",
            "Q  (25%)",
            "H  (30%)"});
            this.Error_list.Location = new System.Drawing.Point(485, 399);
            this.Error_list.Name = "Error_list";
            this.Error_list.Size = new System.Drawing.Size(106, 21);
            this.Error_list.TabIndex = 43;
            this.Error_list.SelectedIndexChanged += new System.EventHandler(this.Error_list_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(603, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.BackgroudLayout.SetRowSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(718, 581);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "EXAMPLES";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(257, 301);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(203, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "CURRENTLY STILL IN DEVELOPMENT";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1324, 587);
            this.Controls.Add(this.BackgroudLayout);
            this.Name = "Form1";
            this.Text = "Form1";
            this.BackgroudLayout.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QrY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NecklaceY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NecklaceX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeparationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeparationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColsDisposition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RowsDisposition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SheetDimensionY)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SheetDimensionX)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel BackgroudLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label InputFile_label;
        private System.Windows.Forms.Label outputDir_label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button DefaultOutput_button;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button DefaultInput_button;
        private System.Windows.Forms.Button BrowseInput_button;
        private System.Windows.Forms.Button BrowseOutput_button;
        private System.Windows.Forms.Button Start_button;
        private System.Windows.Forms.Button SaveOptions_button;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Stroke0_box;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Stroke1_box;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Stroke2_box;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Stroke3_box;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkBox0;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.NumericUpDown SheetDimensionX;
        private System.Windows.Forms.NumericUpDown ColsDisposition;
        private System.Windows.Forms.NumericUpDown RowsDisposition;
        private System.Windows.Forms.NumericUpDown SheetDimensionY;
        private System.Windows.Forms.NumericUpDown QrY;
        private System.Windows.Forms.NumericUpDown NecklaceY;
        private System.Windows.Forms.NumericUpDown NecklaceX;
        private System.Windows.Forms.NumericUpDown SeparationY;
        private System.Windows.Forms.NumericUpDown SeparationX;
        private System.Windows.Forms.ComboBox Unit_list;
        private System.Windows.Forms.ComboBox Error_list;
        private System.Windows.Forms.ToolTip InputLabel_tooltip;
        private System.Windows.Forms.ToolTip OutputDir_tooltip;
    }
}

