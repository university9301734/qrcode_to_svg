﻿using System;
using System.IO;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace QRCode_To_SVG
{
    class SVGManager
    {
        private string[] messages;
        int messagesSize;
        private Options op;
        private string[] tagList;
        private int tagSize;

        public SVGManager(Options op)
        {
            messages = new string[1];
            //messages[0] = "";
            messagesSize = 0;
            tagList = new string[1];
            tagSize = 0;
            ParseInputFile(op.InputPath);
            this.op = op;
        }

        private void ParseInputFile(string path)
        {
            StreamReader r;
            if (File.Exists(path))
                r = new StreamReader(path);
            else
                return;

            using (r)
            {
                while (!r.EndOfStream)
                {
                    string line = r.ReadLine();
                    /*if (line.IndexOf("//") == 0)
                        continue; //skip comment*/

                    if (messagesSize >= messages.Length)
                        messages = Resize(messages);
                    messages[messagesSize] = line;
                    messagesSize++;
                }
            }
        }

        private string[] Resize(string[] a)
        {
            string[] tmp = new string[a.Length * 2];
            for (int i = 0; i < a.Length; i++)
            {
                tmp[i] = a[i];
            }

            return tmp;
        }

        public void WriteSVG()
        {
            int nQr = (int)(op.QRDispoition.X * op.QRDispoition.Y); //qr per files
            int nFiles = messages.Length / nQr; //total number of files
            if (messages.Length < nQr)
                nFiles = 1;
            for (int i = 0; i < nFiles; i++)
            {
                string path = op.OutputPath;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                path = Path.Combine(path, i + "_" + Path.GetFileNameWithoutExtension(Path.GetRandomFileName()) + ".svg");
                StreamWriter w = File.CreateText(path);
                using (w)
                {
                    w.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    string unitOfMeasure = "";
                    switch (op.Measure)
                    {
                        case Options.UnitMeasure.MM:
                            unitOfMeasure = "mm";
                            break;
                        case Options.UnitMeasure.CM:
                            unitOfMeasure = "cm";
                            break;
                        case Options.UnitMeasure.PX:
                            unitOfMeasure = "px";
                            break;
                    }

                    //-----------------------------------------------------------------svg opening line
                    w.WriteLine(OpenTag("svg",
                                        new DictionaryEntry("version", "1.1"),
                                        //new DictionaryEntry("xmlns" , "http://www.w3.org/1999/xhtml"),   //this line is needed for  web browser but inkskape crashes with it
                                        new DictionaryEntry("xmlns:xlink","http://www.w3.org/1999/xlink"),
                                        new DictionaryEntry("x", op.SheetSize.X + unitOfMeasure),
                                        new DictionaryEntry("y", op.SheetSize.Y + unitOfMeasure),
                                        new DictionaryEntry("viewBox", "0 0 " + op.SheetSize.X + " " + op.SheetSize.Y),
                                        new DictionaryEntry("style", "enable-background:new 0 0 " + op.SheetSize.X + " " + op.SheetSize.Y + ";"),
                                        new DictionaryEntry("xml:space", "preserve")
                                        ));

                    //-----------------------------------------------------------------stroke styles
                    /*w.WriteLine(OpenTag("style", new DictionaryEntry("type", "text/css")));
                    w.WriteLine(AddValueToLastOpenTag(".st0{fill:" + (!op.Fill[0] ? "none" : op.Strokes[0].ToString()) + ";stroke:" + op.Strokes[0] + ";stroke-width:0.2;stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                    w.WriteLine(AddValueToLastOpenTag(".st1{fill:" + (!op.Fill[1] ? "none" : op.Strokes[1].ToString()) + ";stroke:" + op.Strokes[1] + ";stroke-width:0.2;stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                    w.WriteLine(AddValueToLastOpenTag(".st2{fill:" + (!op.Fill[2] ? "none" : op.Strokes[2].ToString()) + ";stroke:" + op.Strokes[2] + ";stroke-width:0.2;stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                    w.WriteLine(AddValueToLastOpenTag(".st3{fill:" + (!op.Fill[3] ? "none" : op.Strokes[3].ToString()) + ";stroke:" + op.Strokes[3] + ";stroke-width:0.2;stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                    w.WriteLine(CloseLastOpenTag());*/


                    //----------------------------------------------------------------define every necklace and his qr code
                    //w.WriteLine(OpenTag("defs"));

                    Point[] positions = new Point[(int)(op.QRDispoition.X * op.QRDispoition.Y)];
                    for (int h = 1; h <= op.QRDispoition.X; h++)
                    {
                        for (int g = 1; g <= op.QRDispoition.Y; g++)
                        {
                            positions[h * g - 1] = new Point(h,g);
                        }    
                    }

                    int len = nQr * (i + 1) > messagesSize ? messagesSize : nQr * (i + 1);
                    for (int j = nQr*i; j < len; j++)
                    {
                        int curr_pos = len - j - 1;
                        double x_dim = op.NecklaceSize.X;
                        double y_dim = op.NecklaceSize.Y;
                        double x_sep = op.QRSeparation.X;
                        double y_sep = op.QRSeparation.Y;
                        double x_qr = (x_dim - op.QRSize.X) / 2;
                        double y_qr = -((y_dim - op.QRSize.Y) - x_qr);
                        if (x_dim < y_dim)
                        {
                            x_dim = y_dim;
                            y_dim = op.NecklaceSize.X;
                            x_sep = y_sep;
                            y_sep = op.QRSeparation.X;
                        }
                        w.WriteLine(OpenTag("g",
                                        new DictionaryEntry("id", "necklace" + j),
                                        new DictionaryEntry("transform", "translate(" + ((positions[curr_pos].X - 1) * x_dim + y_sep) + " " + ((positions[curr_pos].Y - 1) * y_dim + y_sep) + ")")/*, //red stroke
                                        new DictionaryEntry("fill", new Color(255,255,255).ToString()),
                                        new DictionaryEntry("width", op.NecklaceSize.X), 
                                        new DictionaryEntry("height", op.NecklaceSize.Y)*/));

                        w.WriteLine(OpenTag("style", new DictionaryEntry("type", "text/css")));
                        w.WriteLine(AddValueToLastOpenTag(".st2{fill:" + (!op.Fill[2] ? "none" : op.Strokes[2].ToString()) + ";stroke:" + op.Strokes[2] + ";stroke-width:0.1;stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                        w.WriteLine(CloseLastOpenTag());

                        double offset = y_dim - y_qr - 2 * x_qr;
                        string value = "M0," + (offset + 1).ToString().Replace(",", ".") + " " +
                                        "L0," + (offset).ToString().Replace(",", ".") + " " +
                                        "L"+(x_dim / offset).ToString().Replace(",", ".") + "," + (offset).ToString().Replace(",", ".") + " " +
                                        "A"+ (offset).ToString().Replace(",", ".")+","+ (offset).ToString().Replace(",", ".")+ " 0 "+ "0,1 " + (x_dim).ToString().Replace(",", ".") + "," + (offset).ToString().Replace(",", ".") + " "+
                                        /*"C" + (x_dim / 2).ToString().Replace(",", ".") + "," + "0" + " "+
                                        "C"+(2 * (x_dim / offset)).ToString().Replace(",", ".") + "," + (offset).ToString().Replace(",", ".") + " " +
                                        "C" + (x_dim).ToString().Replace(",", ".") + "," + (offset).ToString().Replace(",", ".") + " " +*/
                                        "L"+(x_dim).ToString().Replace(",", ".") + "," + (y_dim).ToString().Replace(",", ".") + " " +
                                        "L0" + "," + (y_dim).ToString().Replace(",", ".") + " Z";
                        w.WriteLine(AutoClosingTag("path", new DictionaryEntry("class","st2"), new DictionaryEntry("d", value)));
                        /*w.WriteLine(AutoClosingTag("circle",
                                    new DictionaryEntry("class", "st2"),
                                    new DictionaryEntry("cx", (x_dim / 2).ToString().Replace(",", ".")),
                                    new DictionaryEntry("cy", (offset / 2).ToString().Replace(",", ".")),
                                    new DictionaryEntry("r", "2.2")
                                    ));*/
                        

                        w.WriteLine(OpenTag("g",
                                        new DictionaryEntry("id", "qrcode" + j),
                                        new DictionaryEntry("transform", "translate(" + x_qr + " " + y_qr + ")")
                                        /*new DictionaryEntry("fill", new Color(255, 0, 0).ToString()),
                                        new DictionaryEntry("x", (op.NecklaceSize.X - op.QRSize.X) / 2),
                                        new DictionaryEntry("y", (op.NecklaceSize.Y - op.QRSize.Y) - (op.NecklaceSize.Y - op.QRSize.Y) / 2),
                                        new DictionaryEntry("width", op.QRSize.X), 
                                        new DictionaryEntry("height", op.QRSize.Y)*/));

                        //-----------------------------------------------------------------draw qrcode
                        QRCodeManager qr = new QRCodeManager(messages[j], op.QRError);
                        ArrayList p = qr.FindPolygons();
                        double unit = op.QRSize.X / qr.GetQrSize().X;
                        //----------------------------------------------------------------write the content of the qr for reference
                        w.WriteLine(OpenTag("style", new DictionaryEntry("type", "text/css")));
                        w.WriteLine(AddValueToLastOpenTag(".st0{fill:" + (!op.Fill[0] ? "none" : op.Strokes[0].ToString()) + ";stroke:" + op.Strokes[0] + ";stroke-width:" + unit.ToString().Replace(",", ".") + ";stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                        w.WriteLine(AddValueToLastOpenTag(".st1{fill:" + (!op.Fill[1] ? "none" : op.Strokes[1].ToString()) + ";stroke:" + op.Strokes[1] + ";stroke-width:" + unit.ToString().Replace(",", ".") + ";stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                        w.WriteLine(AddValueToLastOpenTag(".st2{fill:" + (!op.Fill[2] ? "none" : op.Strokes[2].ToString()) + ";stroke:" + op.Strokes[2] + ";stroke-width:" + unit.ToString().Replace(",", ".") + ";stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                        w.WriteLine(AddValueToLastOpenTag(".st3{fill:" + (!op.Fill[3] ? "none" : op.Strokes[3].ToString()) + ";stroke:" + op.Strokes[3] + ";stroke-width:" + unit.ToString().Replace(",", ".") + ";stroke-linecap:bevel;stroke-linejoin:bevel;}"));
                        w.WriteLine(CloseLastOpenTag());

                        w.WriteLine(OpenTag("desc"));
                        w.WriteLine(AddValueToLastOpenTag("content of qr: "+messages[j]));
                        w.WriteLine(CloseLastOpenTag());


                        //-----------------------------------------------------------------draw the strokes
                        
                        for (int k = 0; k < p.Count; k++)
                        {   
                            string points = "";
                            ArrayList pointList = (ArrayList)((DictionaryEntry)p[k]).Key;
                            for (int z = 0; z < pointList.Count; z++)
                            {
                                Point pp = (Point)(pointList[z]);
                                points += ( pp.X * unit).ToString().Replace(",", ".") + "," + (pp.Y * unit).ToString().Replace(",", ".") + " ";
                            }
                            string stroke = "st" + (int)((DictionaryEntry)p[k]).Value; //expected value of 0 and 1 for black and blu
                            w.WriteLine(AutoClosingTag("polygon",
                                                        new DictionaryEntry("class", stroke),
                                                        new DictionaryEntry("points", points + " ")
                                                            ));
                        }


                        w.WriteLine(CloseLastOpenTag()); //close qrcode g
                        w.WriteLine(CloseLastOpenTag()); //close necklace g                    
                    }

                    //w.WriteLine(CloseLastOpenTag()); //close defs

                    /*w.WriteLine(OpenTag("g"));
                    w.WriteLine(AutoClosingTag("use", new DictionaryEntry("x", "1"), new DictionaryEntry("y", "1"), new DictionaryEntry("xlink:href", "#necklace0")));
                    w.WriteLine(AutoClosingTag("use", new DictionaryEntry("x", "10"), new DictionaryEntry("y", "10"), new DictionaryEntry("xlink:href", "#necklace1")));*/

                    //--------------------------------------------------------------close remaining open tags
                    w.Write(CloseAllOpenTags());
                    Debug.WriteLine("+++++++++" + w.ToString());
                }
            }
        }

        private string OpenTag(string tagName, params DictionaryEntry[] attributes)
        {
            if (tagSize >= tagList.Length)
                tagList = Resize(tagList);
            tagList[tagSize] = tagName;
            tagSize++;

            string s = "";
            for (int i = 0; i < tagSize-1; i++)
            {
                s += "\t";
            }

            s += "<" + tagName;
            for (int i = 0; i < attributes.Length; i++)
            {
                s += " " + attributes[i].Key + "=\"" + attributes[i].Value + "\"";
            }
            s += ">";

            return s;
        }

        private string AddValueToLastOpenTag(string value)
        {
            for (int i = 0; i < tagSize; i++)
            {
                value = "\t" + value;
            }

            return value;
        }

        private string CloseAllOpenTags()
        {
            string s = "";
            for (int i = tagSize-1; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    s += "\t";
                }
                s += "</" + tagList[i] + ">\n";
            }
            s += "</" + tagList[0] + ">";
            tagSize = 0;
            tagList = new string[1];
            return s;
        }

        private string CloseLastOpenTag()
        {
            if (tagSize > 0)
            {
                string s = "";
                for (int i = 0; i < tagSize-1; i++)
                {
                    s += "\t";
                }
                return s + "</" + tagList[--tagSize] + ">";
            }

            return ""; //may cause problems because it can write a blank line in the middle of the file
        }

        private string AutoClosingTag(string tagName, params DictionaryEntry[] attributes)
        {
            string s = "";
            for (int i = 0; i < tagSize; i++)
            {
                s += "\t";
            }
            s += "<" + tagName;
            for (int i = 0; i < attributes.Length; i++)
            {
                s += " " + attributes[i].Key + "=\"" + attributes[i].Value + "\"";
            }

            return s + "/>";
        }
    }
}
