﻿using System;
using QuickGraph;
using ZXing.QrCode.Internal;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCode_To_SVG
{
    class QRCodeManager
    {
        private static readonly int[,] OUTLINE_KER = new int[3, 3] { { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } }; //kernel for outline extraction
        private QRCode q;
        //private Size s;
        private enum StrokeColor { DONT_CARVE_INSIDE=0, CARVE_INSIDE=1 }

        public QRCodeManager(string message, ErrorCorrectionLevel level/*, Size s*/)
        {
            q = ZXing.QrCode.Internal.Encoder.encode(message, level);
            //this.s = s;
        }

        public ArrayList FindPolygons()
        {
            ArrayList poligons = new ArrayList();
            Debug.WriteLine(q.Matrix.ToString());
            byte[,] h = FindHoles(q.Matrix);
            byte[,] b = ExpandMatrix(q.Matrix, 1);

            /*Debug.WriteLine(MatrixToString(b));
            Debug.WriteLine(MatrixToString(h));*/
            b = AddMatrix(b, h);           
            b = ExpandMatrix(b, 3);
            h = ExpandMatrix(h, 3);
            b = KernelConvolution(b, OUTLINE_KER);
            h = KernelConvolution(h, OUTLINE_KER);

            //adjust the QR markers on b and h
            int l = (int)Math.Floor(Math.Sqrt(b.Length));
            /*SetFrame(b, new Point(6, 6), 9, 9);
            SetFrame(b, new Point(l-15, 6), 9, 9);
            SetFrame(b, new Point(6, l-15), 9, 9);*/
            SetFrame(h, new Point(5, 5), 11, 11, 0);
            SetFrame(h, new Point(l - 16, 5), 11, 11, 0);
            SetFrame(h, new Point(5, l - 16), 11, 11, 0);
            if (q.Matrix.Height > 21)
                SetFrame(h, new Point(l - 22, l - 22), 5, 5, 0);
            /*SetFrame(h, new Point(3, 3), 15, 15, 0);
            SetFrame(h, new Point(l - 18, 3), 15, 15, 0);
            SetFrame(h, new Point(3, l - 18), 15, 15, 0);
            SetFrame(h, new Point(2, 2), 16, 16, 1);
            SetFrame(h, new Point(l - 18, 2), 16, 16, 1);
            SetFrame(h, new Point(2, l - 18), 16, 16, 1);*/

            //Debug.WriteLine(MatrixToString(h));

            for (int i = 0; i < l; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    if (b[i, j] == 1)
                        poligons.Add(new DictionaryEntry(FindPoligon(b, new Point(j,i)), StrokeColor.CARVE_INSIDE));
                }
            }

            for (int i = 0; i < l; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    if (h[i, j] == 1)
                        poligons.Add(new DictionaryEntry(FindPoligon(h, new Point(j, i)), StrokeColor.DONT_CARVE_INSIDE));
                }
            }

            //finally make sure the alignment squares are correctly diplayed
            
            b = new byte[l, l];
            if (q.Matrix.Height > 21)
            {
                SetFrame(b, new Point(l - 22, l - 22), 5, 5);
            }
            SetFrame(b, new Point(6, 6), 9, 9);
            SetFrame(b, new Point(l-15, 6), 9, 9);
            SetFrame(b, new Point(6, l-15), 9, 9);
            for (int i = 0; i < l; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    if (b[i, j] == 1)
                        poligons.Add(new DictionaryEntry(FindPoligon(b, new Point(j, i)), StrokeColor.CARVE_INSIDE));
                }
            }
            
            //Debug.WriteLine(MatrixToString(h));

            return poligons;
        }

        private void SetFrame(byte[,] m, Point coord, int xlen, int ylen ,byte val = 1)
        {
            for (int i = coord.Y; i < ylen+coord.Y; i++)
            {
                m[i, coord.X] = val;
                m[i, coord.X + xlen - 1] = val;
            }

            for (int j = coord.X; j < ylen+coord.X; j++)
            {
                m[coord.Y, j] = val;
                m[coord.Y + ylen - 1, j] = val;
            }
        }

        private byte[,] FindHoles(ByteMatrix m)
        {
            byte[,] b = ExpandMatrix(m, 1);
            b = AddMatrixBorder(b, 2);
            for (int k = 0; k < 9; k++)
            {
                for (int i = 1; i < m.Height + 1; i++)
                {
                    for (int j = 1; j < m.Height + 1; j++)
                    {
                        if (b[i, j] == 0)
                        {
                            if (b[i - 1, j - 1] == 2 || b[i - 1, j] == 2 || b[i - 1, j + 1] == 2 || b[i, j - 1] == 2 || b[i, j + 1] == 2 || b[i + 1, j - 1] == 2 || b[i + 1, j] == 2 || b[i + 1, j + 1] == 2)
                                b[i, j] = 2;
                        }
                    }
                }
            }

            byte[,] o = new byte[m.Height, m.Height];
            for (int i = 0; i < m.Height; i++)
            {
                for (int j = 0; j < m.Height; j++)
                {
                    if (b[i+1, j+1] == 0)
                        o[i, j] = 1;
                    else
                        o[i, j] = 0;
                }
            }

            return o;
        }

        private ArrayList FindPoligon(byte[,] m, Point start)
        {
            ArrayList poligon = new ArrayList();
            int l = (int)Math.Floor(Math.Sqrt(m.Length));
            poligon.Add(new Point(start.X, start.Y));
            bool done = false ;
            m[start.Y, start.X] = 0;
            start.MoveRigth();
            string nextMove = "R";
            string lastMove = "R";
            while (!done)
            {
                m[start.Y, start.X] = 0;
                if (start.X+1 < l && m[start.Y, start.X + 1] == 1)
                    nextMove = "R";
                else if (start.Y + 1 < l && m[start.Y + 1, start.X] == 1)
                    nextMove = "D";
                else if (start.X - 1 >= 0 && m[start.Y, start.X - 1] == 1)
                    nextMove = "L";
                else if (start.Y - 1 >= 0 && m[start.Y - 1, start.X ] == 1)
                    nextMove = "U";
                else
                {
                    done = true;
                    break;
                }

                if (!lastMove.Equals(nextMove))
                {
                    poligon.Add(new Point(start.X, start.Y));
                    //Debug.WriteLine(start.ToString()+ " " + poligon.Contains(start));
                }
                lastMove = nextMove;

                switch (nextMove)
                {
                    case "R":
                        start.MoveRigth();
                        break;
                    case "D":
                        start.MoveDown();
                        break;
                    case "L":
                        start.MoveLeft();
                        break;
                    case "U":
                        start.MoveUp();
                        break;
                }   
            }

            return poligon;
        }

        private byte[,] AddMatrixBorder(byte[,] m, byte bordervalue = 0)
        {
            int l = (int)Math.Floor(Math.Sqrt(m.Length)) + 2;
            byte[,] mat = new byte[l,l];
            for (int i = 0; i < l; i++)
            {
                mat[i, 0] = bordervalue;
                mat[i, l - 1] = bordervalue;
                mat[0, i] = bordervalue;
                mat[l - 1, i] = bordervalue;
            }

            for (int i = 1; i < l-1; i++)
            {
                for (int j = 1; j < l-1; j++)
                {
                    mat[i, j] = m[i - 1, j - 1];
                }
            }

            return mat;
        }

        private byte[,] ExpandMatrix(ByteMatrix original, int factor = 3)
        {
            int l = original.Height;
            byte[,] m = new byte[l * factor, l * factor];
            for (int k = 0; k < factor; k++)
            {
                for (int z = 0; z < factor; z++)
                {
                    for (int i = k; i < l * factor; i = i + factor)
                    {
                        for (int j = z; j < l * factor; j = j + factor)
                        {
                            m[i, j] = (byte)original[j / factor, i / factor];
                        }
                    }
                }
            }
            return m;
        }

        private byte[,] ExpandMatrix(byte[,] original, int factor = 3)
        {
            int l = (int)Math.Floor(Math.Sqrt(original.Length));
            byte[,] m = new byte[l * factor, l * factor];
            for (int k = 0; k < factor; k++)
            {
                for (int z = 0; z < factor; z++)
                {
                    for (int i = k; i < l * factor; i = i + factor)
                    {
                        for (int j = z; j < l * factor; j = j + factor)
                        {
                            m[i, j] = original[i / factor, j / factor];
                        }
                    }
                }
            }
            return m;
        }

        private byte[,] KernelConvolution(byte[,] inMat, int[,] ker)
        {
            byte[,] b = inMat;
            int l = (int)Math.Floor(Math.Sqrt(b.Length));
            inMat = AddMatrixBorder(inMat);

            for (int i = 1; i < l+1; i++)
            {
                for (int j = 1; j < l+1; j++)
                {
                    int t = 0;
                    int len = (int)Math.Floor(Math.Sqrt(ker.Length));
                    for (int z = 0; z < len; z++)
                    {
                        for (int w = 0; w < len; w++)
                        {
                            t += (inMat[i-1+z,j-1+w] * ker[z, w]);
                        }
                    }
                    b[i - 1, j - 1] = t>0? (byte)1 : (byte)0;
                }
            }
            return b;
        }

        private byte[,] AddMatrix(byte[,] m1, byte[,] m2)
        {
            int l1 = (int)Math.Floor(Math.Sqrt(m1.Length));
            int l2 = (int)Math.Floor(Math.Sqrt(m2.Length));
            if (l1 != l2)
                return m1;

            for (int i = 0; i < l1; i++)
            {
                for (int j = 0; j < l1; j++)
                {
                    m1[i, j] += m2[i, j];
                }
            }

            return m1;
        }

        public Size GetQrSize()
        {
            int i = q.Matrix.Height * 3;
            return new Size(i,i);
        }

        public string MatrixToString(byte[,] m)
        {
            string s = "";
            int l =(int) Math.Floor(Math.Sqrt(m.Length));
            for (int i = 0; i < l ; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    s += " " + m[i, j];
                }
                s += "\n";
            }
            return s;
        }
    }
}
