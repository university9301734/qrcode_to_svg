﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCode_To_SVG
{
    struct Size
    {
        public enum UnitOfMeasure {NONE, PIXEL, MM, CM }
        private double x;
        private double y;
        private UnitOfMeasure m;

        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double Y
        {
            get { return y; }
            set { y = value; }
        }
        public UnitOfMeasure Measure
        {
            get { return m; }
        }

        public Size(double x, double y, UnitOfMeasure measure = UnitOfMeasure.NONE)
        {
            this.x = x;
            this.y = y;
            m = measure;
        }

        public Size(double x, double y, string unitOfMeasure)
        {
            this.x = x;
            this.y = y;
            if (unitOfMeasure.Equals("mm"))
                m = UnitOfMeasure.MM;
            else if (unitOfMeasure.Equals("cm"))
                m = UnitOfMeasure.CM;
            else if (unitOfMeasure.Equals("px"))
                m = UnitOfMeasure.PIXEL;
            else
                m = UnitOfMeasure.NONE;
        }

        public override string ToString()
        {
            string s = "none";
            if (m == UnitOfMeasure.MM)
                s = "mm";
            else if (m == UnitOfMeasure.CM)
                s = "cm";
            else if (m == UnitOfMeasure.PIXEL)
                s = "px";
            return "(" +X+ "," +Y+","+s+ ")";
        }
    }
}
